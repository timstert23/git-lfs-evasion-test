
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Config/Weapon")]
public class WeaponConfig : ScriptableObject
{
	new public string name = string.Empty;
	public string animationName = string.Empty;
	public float speedDecrease;
	public float bulletTravelDistance;
}
