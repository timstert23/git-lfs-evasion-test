
using System.Collections.Generic;
using UnityEngine;

public class PrefabCache
{
	private static Dictionary<string, Object> cache = new Dictionary<string, Object>();

	public static T Get<T>(string path) where T : Object
	{
		// Note: I use a try catch to not waste time doing an lookup
		// with Contains() only to have to do another lookup to retrieve
		// the value. Instead, just retrieve the value and if not found, 
		// catch the error and store it in cache.
		//
		
        try 
        {
            return cache[path] as T;
        }
        catch (KeyNotFoundException)
        {
			Object o = Resources.Load(path);
			cache[path] = o;
			return o as T;
        }
	}
}
