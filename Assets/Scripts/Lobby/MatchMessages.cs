﻿using System;
using Mirror;

/// <summary>
/// Match message to be sent to the server
/// </summary>
public struct ServerMatchMessage : NetworkMessage
{
	public ServerMatchOperation serverMatchOperation;
	public string matchId;
}

/// <summary>
/// Match message to be sent to the client
/// </summary>
public struct ClientMatchMessage : NetworkMessage
{
	public ClientMatchOperation clientMatchOperation;
	public string matchId;
	public MatchInfo[] matchInfos;
	public PlayerInfo[] playerInfos;
}

/// <summary>
/// Information about a match
/// </summary>
[Serializable]
public struct MatchInfo
{
	public string matchId;
	public byte players;
	public byte maxPlayers;
}

/// <summary>
/// Information about a player
/// </summary>
[Serializable]
public struct PlayerInfo
{
	public EquipSet equipSet;
	public string playerName;
	public string matchId;
}


/// <summary>
/// Match operation to execute on the server
/// </summary>
public enum ServerMatchOperation : byte
{
	None,
	Create,
	Cancel,
	Start,
	Join,
	Leave,
	Ready
}

/// <summary>
/// Match operation to execute on the client
/// </summary>
public enum ClientMatchOperation : byte
{
	None,
	List,
	Created,
	Cancelled,
	Joined,
	Departed,
	UpdateRoom,
	Started
}