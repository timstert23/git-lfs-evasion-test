
using System;
using Mirror;
using UnityEngine;

public class Player : NetworkBehaviour
{
	[SyncVar]
	public Vector3 m_InitialLookAt;

	[SerializeField]
	private Transform m_UpperBody;

	[SerializeField]
	private Transform m_Body;

	[SerializeField]
	private Transform m_WeaponMount;

	// TODO: Currently using the legacy animation
	// system. Should switch to new system to handle
	// state machine
	//
	[SerializeField]
	private Animation m_Animation;

	[SerializeField]
	private CharacterController m_Controller;

	[SerializeField]
	private const float BASE_WALK_SPEED = 10.2f;

	private Vector3 m_TargetLookAt;

	private Vector3 m_CurrentLookAt;
	
	private float m_WalkSpeed;

	private Weapon m_SelectedWeapon;


	public void Init(EquipSet equipSet)
	{
		GameObject avatarPrefab = GameApp.LoadPlayer(ref equipSet, ref m_SelectedWeapon, gameObject);
		AvatarCreator.MountMultiSkinnedMesh(gameObject, avatarPrefab);
		
		ChangeWeapon(m_SelectedWeapon);
		SetIdleState();

		m_TargetLookAt = m_InitialLookAt;
		m_CurrentLookAt = m_TargetLookAt;
	}

	public void Move(Vector3 velocity)
	{
		m_Controller.Move(velocity * m_WalkSpeed * Time.deltaTime + Physics.gravity);
	}

	public void PlayAudio()
	{
		// TODO: This is a callback from animation event system.
	}

	public void PlayFootStep1()
	{
		// TODO: This is a callback from animation event system.
	}

	public void PlayFootStep2()
	{
		// TODO: This is a callback from animation event system.
	}

	private void ChangeWeapon(Weapon new_weapon)
	{
		if (m_SelectedWeapon != null)
			m_SelectedWeapon.SetActive(false);

		m_SelectedWeapon = new_weapon;
		m_SelectedWeapon.SetActive(true);
		m_SelectedWeapon.Mount(transform, m_WeaponMount);

		SetAnimationProperties(m_SelectedWeapon.GetAnimationName());
		CalcWalkSpeed();
	}

	public Weapon GetCurrentWeapon()
	{
		return m_SelectedWeapon;
	}

	public void CalcWalkSpeed()
	{
		m_WalkSpeed = BASE_WALK_SPEED * (1f + m_SelectedWeapon.GetSpeedDecrease());
	}

	void Update()
	{
		UpdateUpBody();
	}

	private void UpdateUpBody()
	{
		m_CurrentLookAt = Vector3.Lerp(m_CurrentLookAt, m_TargetLookAt, 0.3f);

		Vector3 dir = Vector3.Normalize(m_CurrentLookAt - m_UpperBody.position);
		float pitch = -Utils.AngleAroundAxis(transform.forward, dir, transform.right) + 5f;
		m_UpperBody.localRotation = Quaternion.Euler(270f, 90f, 0f) * Quaternion.Euler(0, 0, pitch);
	}

	public void LookAt(Vector3 lookAt)
	{
		m_TargetLookAt = lookAt;
	}

	// TODO: Temp animation state machine, Rework animation logic, probably should use the newer animation
	// system.
	//
	public void ChangeAnimationState(int animationState)
	{
		switch (animationState)
		{
			case 0: SetIdleState(); break;
			case 1: SetMovingState(); break;
		}
	}

	private void SetAnimationProperties(string weaponAnimationSuffix)
	{
		this.SplitBodyAnimation(weaponAnimationSuffix, "Idle01_");
		this.SplitBodyAnimation(weaponAnimationSuffix, "Run01_");
		this.SplitBodyAnimation(weaponAnimationSuffix, "Fire01_");

		if (weaponAnimationSuffix == "Knife")
			this.SplitBodyAnimation(weaponAnimationSuffix, "Fire02_");

		else if (weaponAnimationSuffix == "Protoss")
		{
			this.SplitBodyAnimation(weaponAnimationSuffix, "PreFire01_");
			this.SplitBodyAnimation(weaponAnimationSuffix, "SurFire01_");
			this.SplitBodyAnimation(weaponAnimationSuffix, "JumpStar01_");
			this.SplitBodyAnimation(weaponAnimationSuffix, "JumpIdle01_");
			this.SplitBodyAnimation(weaponAnimationSuffix, "JumpEnd01_");
		}
		else if (weaponAnimationSuffix != "Knife2" && weaponAnimationSuffix != "Knife3")
			this.SplitBodyAnimation(weaponAnimationSuffix, "Reload01_");

		this.SplitBodyAnimation(string.Empty, "Knock01");
		this.SplitBodyAnimation(string.Empty, "Push01");
		this.SplitBodyAnimation(string.Empty, "Throw01_HandGun");
	}

	public void SplitBodyAnimation(string weaponAnimationSuffix, string animName)
	{
		string name = animName + weaponAnimationSuffix;

		AnimationState lowerBody = m_Animation[name + "_LowerBody"];
		lowerBody.AddMixingTransform(m_Body);
		lowerBody.layer = 0;

		AnimationState upperBody = m_Animation[name + "_UpperBody"];
		upperBody.AddMixingTransform(m_UpperBody);
		upperBody.layer = 2;
	}

	private void SetIdleState()
	{
		AnimationState lowerBody = m_Animation["Idle01_" + m_SelectedWeapon.GetAnimationName() + "_LowerBody"];
		lowerBody.layer = 0;
		lowerBody.speed = 1f;
		lowerBody.wrapMode = WrapMode.Loop;
		m_Animation.CrossFade(lowerBody.name, 0.3f);

		AnimationState upperBody = m_Animation["Idle01_" + m_SelectedWeapon.GetAnimationName() + "_UpperBody"];
		upperBody.layer = 2;
		upperBody.speed = 1f;
		upperBody.wrapMode = WrapMode.Loop;
		m_Animation.CrossFade(upperBody.name, 0.15f);
	}

	private void SetMovingState()
	{
		AnimationState lowerBody = m_Animation["Run01_" + m_SelectedWeapon.GetAnimationName() + "_LowerBody"];
		lowerBody.wrapMode = WrapMode.Loop;
		m_Animation.CrossFade(lowerBody.name, 0.3f);

		AnimationState upperBody = m_Animation["Run01_" + m_SelectedWeapon.GetAnimationName() + "_UpperBody"];
		upperBody.wrapMode = WrapMode.Loop;
		upperBody.layer = 2;
		upperBody.speed = m_WalkSpeed / BASE_WALK_SPEED;

		m_Animation.CrossFade(upperBody.name, 0.3f);
	}
	//
}
