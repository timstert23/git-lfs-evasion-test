
using UnityEngine;

public class BulletBehaviourBase : MonoBehaviour
{
    public ParticleSystem m_ImpactParticleSystem;

    public Vector3 m_Forward;

    public float m_Speed;

    private float m_StartTime;

    private float m_AttackRange = 100f;

    public virtual void PlayOnImpact()
    {
        m_ImpactParticleSystem.Play();
    }

    public ParticleSystem GetImpactPS()
    {
        return m_ImpactParticleSystem;
    }

    public void Init()
    {
        m_StartTime = Time.time;
    }

    void Update()
    {
        transform.Translate(m_Forward * m_Speed * Time.deltaTime, Space.World);

        if ((Time.time - m_StartTime) * m_Speed > m_AttackRange)
            DestroyBullet(false);

    }

    public void DestroyBullet(bool bDestoyGameObject = true)
    {
        if (bDestoyGameObject)
        {
            Destroy(gameObject);
            return;
        }

        // TODO: This behaviour should be derived into separate sub classes
        // (a bullet script should know it's own components and cache them).
        //        
        ParticleSystem ps = gameObject.GetComponent<ParticleSystem>();

        if (ps != null)
        {
            ps.Clear(true);
            ParticleSystem.EmissionModule emission = ps.emission;
            emission.enabled = false;
            ps.Stop(true);
        }

		TrailRenderer ts = gameObject.GetComponent<TrailRenderer>();

        if (ts != null)
            ts.Clear();

        gameObject.SetActive(false);
    }

    public void SetFastAimTargetPos(Vector3 fastAimTargetPos)
    {
        m_AttackRange = Vector3.Distance(fastAimTargetPos, base.transform.position);
    }

}
