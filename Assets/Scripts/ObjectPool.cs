
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool<T> where T : Component
{
	private GameObject m_Prefab;

	private List<T> m_Pool = new List<T>();

	// Note: We ask for the prefab of type T to ensure that the gameobject
	// 		 itself contains the component T.
	//
	public ObjectPool(T prefab, int initialSize)
	{
		m_Prefab = prefab.gameObject;

		for (int i = 0; i < initialSize; ++i)
			m_Pool.Add(Instantiate(m_Prefab));
	}

	private T Instantiate(GameObject prefab)
	{
		GameObject obj = UnityEngine.Object.Instantiate(prefab);
		obj.SetActive(false);
		return obj.GetComponent<T>();
	}

	public T CreateObject(Vector3 position, Quaternion rotation)
	{
		// TODO: Linear search is :P
		//
		T selected = null;
		foreach (T component in m_Pool)
		{
			if (component.gameObject.activeInHierarchy)
				continue;

			selected = component;
		}

		if (selected == null)
		{
			selected = Instantiate(m_Prefab);
			m_Pool.Add(selected);
		}

		selected.transform.position = position;
		selected.transform.rotation = rotation;
		selected.gameObject.SetActive(true);

		return selected;
	}

	public void Trim()
	{
		for (int i = 0; i < m_Pool.Count; ++i)
		{
			if (m_Pool[i].gameObject.activeInHierarchy)
				continue;

			UnityEngine.Object.Destroy(m_Pool[i].gameObject);
			m_Pool.RemoveAt(i);
		}
	}
}