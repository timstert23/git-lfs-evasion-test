
using UnityEngine;

public class Weapon : WeaponBase
{
    // TODO: Put this in the weapon config once we have bullet prefabs (insert :kekw: here)
    [SerializeField]
    private BulletBehaviourBase m_BulletPrefab;

    [SerializeField]
    private ParticleSystem m_HitEffectPrefab;

    [SerializeField]
    float m_Thrust = 700f;

    private ObjectPool<ParticleSystem> m_ParticlePool;

    private ObjectPool<BulletBehaviourBase> m_BulletPool;

    private Vector3 m_AimTargetPos;

    void Start()
    {
        // Note: This may seem super dumb and that's because it is.
        // But apparently modifying a prefab in a script actually SAVES the prefab resource
        // thanks unity. So we need to create a cloned object to use as a prefab.
        //
        GameObject prefabClone = Instantiate(m_HitEffectPrefab.gameObject);
        ParticleSystem ps = prefabClone.GetComponent<ParticleSystem>();

        ParticleSystem.MainModule main = ps.main;
        ParticleSystem.EmissionModule emission = ps.emission;

        main.playOnAwake = false;
        main.loop = false;
        emission.enabled = false;
        ps.Stop(true);
        prefabClone.AddComponent<DeavtivateTimer>();

        m_ParticlePool = new ObjectPool<ParticleSystem>(ps, 3);

        m_BulletPool = new ObjectPool<BulletBehaviourBase>(m_BulletPrefab, 3);
    }

    public override void Fire()
    {
        base.Fire();

        Transform firePoint = GetFirePoint();
        BulletBehaviourBase bullet = CreateBullet(firePoint);



        Debug.DrawRay(firePoint.position, firePoint.forward * 3, Color.red, 2f);

        GameObject target_Obj;
        Vector3 hitPoint = GetScreenAimTargetPosition(out target_Obj, 64, 300f);

        bullet.Init();
        bullet.m_Forward = (hitPoint - firePoint.position).normalized;
        bullet.m_Speed = m_Thrust;

        // TODO: This is where we'll determine what the bullet will hit.
        // If it is a player object, then we will send hit info (damage, crit, etc.)
        // to the recipient object.
        //
        bullet.SetFastAimTargetPos(hitPoint);
        CreateHitParticleEffect(hitPoint);
    }

    private BulletBehaviourBase CreateBullet(Transform transform)
    {
        return m_BulletPool.CreateObject(transform.position, transform.rotation);
    }

    public Vector3 GetWeaponAimTargetPosition(Ray weapon_ray, out GameObject hitObj, int layerMask = 1536, float max_range = 300f)
    {
        hitObj = null;
        Ray ray = weapon_ray;
        RaycastHit raycastHit;
        Vector3 point;
        if (Physics.Raycast(ray, out raycastHit, max_range, layerMask))
        {
            point = raycastHit.point;
            hitObj = raycastHit.transform.gameObject;
        }
        else
        {
            float distance = 1000f;
            point = ray.GetPoint(distance);
        }
        return point;
    }


    private void CreateHitParticleEffect(Vector3 pos)
    {
        ParticleSystem ps = m_ParticlePool.CreateObject(pos, Quaternion.identity);
        ParticleSystem.MainModule main = ps.main;
        ParticleSystem.EmissionModule emission = ps.emission;
        emission.enabled = true;

        ps.Clear(true);
        ps.Play(true);
        ps.gameObject.GetComponent<DeavtivateTimer>().Init(main.duration);
    }
    public Vector3 GetScreenAimTargetPosition(out GameObject hitObj, int layerMask, float max_range = 300f)
    {
        hitObj = null;
        float x = (float)Screen.width / 2f + UnityEngine.Random.Range(30f / 2f, 30f / 2f);
        float y = (float)Screen.height / 2f + UnityEngine.Random.Range(30f / 2f, 30f / 2f);
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(x, y, 0f));
        Vector3 point = ray.GetPoint(max_range);
        TPSSimpleCameraScript component = Camera.main.GetComponent<TPSSimpleCameraScript>();
        if (component != null)
        {
            float cameraDistanceFromPlayerXZ = component.GetCameraDistanceFromPlayerXZ();
            Vector3 point2 = ray.GetPoint(cameraDistanceFromPlayerXZ);
            ray = new Ray(point2, ray.direction);
        }
        RaycastHit raycastHit;
        if (Physics.Raycast(ray, out raycastHit, max_range, layerMask))
        {
            point = raycastHit.point;
            hitObj = raycastHit.transform.gameObject;
        }
        else
        {
            float distance = 1000f;
            point = ray.GetPoint(distance);
        }
        m_AimTargetPos = point;
        return point;
    }
}
