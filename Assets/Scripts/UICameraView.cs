
using UnityEngine;
using UnityEngine.EventSystems;

public class UICameraView : MonoBehaviour, IDragHandler
{
	private Vector2 m_Delta = Vector2.zero;

	public void OnDrag(PointerEventData eventData)
	{
		m_Delta += eventData.delta;
	}

    public Vector2 GetDelta()
    {
        Vector2 delta = m_Delta;
		m_Delta = Vector2.zero;
        return delta;
    }
}
