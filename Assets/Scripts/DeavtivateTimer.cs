
using UnityEngine;

public class DeavtivateTimer : MonoBehaviour
{
    private float m_StartTime;

    private float m_Duration;

    public void Init(float duration)
    {
		m_Duration = duration;
        m_StartTime = Time.time;
    }

    void Update()
    {
        if (Time.time - m_StartTime < m_Duration)
            return;

        // Destroy self
        //
        gameObject.SetActive(false);
    }
}
