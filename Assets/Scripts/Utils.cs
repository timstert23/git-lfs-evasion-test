
using System.Linq;
using UnityEngine;
using Random = System.Random;

public class Utils
{
	public static float AngleAroundAxis(Vector3 dirA, Vector3 dirB, Vector3 axis)
	{

		dirA -= Vector3.Project(dirA, axis);
		dirB -= Vector3.Project(dirB, axis);
		float angle = Vector3.Angle(dirA, dirB);
		float negate = Vector3.Dot(axis, Vector3.Cross(dirA, dirB)) >= 0f ? 1f : -1f;

		return angle * negate;
	}

	public static void ObjectSetActiveRecursively(GameObject rootObject, bool active)
	{
		rootObject.SetActive(active);
		foreach (Transform obj in rootObject.transform)
		{
			Transform transform = obj;
			Utils.ObjectSetActiveRecursively(transform.gameObject, active);
		}
	}

	public static bool AreVectorsEqual(Vector3 v1, Vector3 v2, float epsilon)
	{
		return Mathf.Abs(v1.x - v2.x) <= epsilon &&
			   Mathf.Abs(v1.y - v2.y) <= epsilon &&
			   Mathf.Abs(v1.z - v2.z) <= epsilon;
	}

	public static bool IsMoving(Vector3 v)
	{
		return v.x * v.x + v.y * v.y + v.z * v.z > 0f;
	}

	private static Random random = new Random();
	public static string RandomString(int length)
	{
		const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		return new string(Enumerable.Repeat(chars, length)
		.Select(s => s[random.Next(s.Length)]).ToArray());
	}

	/// <summary>
	/// Returns true if the room ID is valid (i.e. the lobby ID is not in use), otherwise it returns false
	/// </summary>
	/// <param name="matchIDs"></param>
	/// <param name="generatedMatchID"></param>
	/// <returns></returns>
	public static bool ValidateRoomID(MatchInfo[] matchIDs, string generatedMatchID)
	{
		foreach (var id in matchIDs)
		{
			if (id.matchId == generatedMatchID)
			{ return false; }
		}
		return true;
	}
}
