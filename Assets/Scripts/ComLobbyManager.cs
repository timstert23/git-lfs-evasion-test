using System;
using Mirror;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ComLobbyManager : NetworkBehaviour
{
	//TODO: Add a listener in ComNetManager.cs, only run init code then
	public static event Action notifyClientReady;
	public static event Action notifyHostReady;
	public static event Action notifyServerReady;

	private event Action notifyStartGameAvailable; //This event gets triggered when atleast 2 people are in the lobby

	[SerializeField]
	private Button m_HostButton, m_JoinButton, m_StartGameButton;

	[SerializeField]
	private NetworkManager m_NetManager;

	[SerializeField]
	private TextMeshProUGUI m_RoomID_txt, m_playersInLobby_txt;

	private const int m_MinPlayers = 2; //1v1
	private const int m_MaxPlayers = 8; //4v4

	[SerializeField]
	[SyncVar (hook = nameof(PlayerCountHook))]
	private int m_PlayersInLobby = 0;

	//1. Send command to server telling it the player has been added
	//2. Await a TargetRpc telling the client that told the server the player has been added, this rpc will just update the player count for now

	private int m_LobbyID;
	private string serverIP = "localhost";
	private int[] m_activeRooms;

	//BUG: "Host" is not hosting the server, the client cannot connect
	private void Start()
	{
		this.m_HostButton.onClick.AddListener(OnClickHostBtn);
		this.m_JoinButton.onClick.AddListener(OnClickJoinBtn);
		this.m_StartGameButton.onClick.AddListener(OnClickStartGameBtn);
		this.notifyStartGameAvailable += OnStartGameAvailable;
		this.m_NetManager.networkAddress = this.serverIP;
	}
	public override void OnStartAuthority()
	{
		base.OnStartAuthority();
		Debug.Log("on start authority");
	}

	#region Game host
	private void OnClickStartGameBtn()
	{

	}

	private void OnStartGameAvailable()
	{
		if (this.m_PlayersInLobby >= m_MinPlayers && this.m_PlayersInLobby <= m_MaxPlayers)
		{
			this.m_StartGameButton.interactable = true;
			notifyHostReady?.Invoke();
		}
		else
		{
			this.m_StartGameButton.interactable = false;
		}
	}

	private void OnClickHostBtn()
	{
		//Handle player hosting the game
		if (!isPlayerNetworkingNow())
		{
			this.GenLobbyID();
			this.UpdateLobbyID();

			//Click host to host, click it again to stop and make the join button pressable again
			this.m_JoinButton.interactable = (this.m_JoinButton.interactable == true) ? false : true;

			if (!this.m_JoinButton.interactable)
			{
				this.m_NetManager.StartHost();
				this.m_PlayersInLobby++;
				this.notifyStartGameAvailable?.Invoke();
			}
			else
			{
				this.m_NetManager.StopHost();
				this.m_PlayersInLobby = 0;
				// this.m_playersInLobby_txt.text = $"Players in lobby: {this.m_PlayersInLobby}";
				this.m_RoomID_txt.text = string.Empty;
				this.m_StartGameButton.interactable = false;
			}
		}
	}
	#endregion

	#region Player Join
	private void OnClickJoinBtn()
	{
		if (!NetworkClient.active)
		{
			this.m_NetManager.StartClient();
			this.m_PlayersInLobby++;
			notifyClientReady?.Invoke();
		}
	}
	#endregion

	#region Misc lobby
	public void PlayerCountHook(int _, int newPlayerCount)
	{
		Debug.Log("hook: old: " + _ + " new: " + newPlayerCount);
		this.m_playersInLobby_txt.text = $"Players in lobby: {this.m_PlayersInLobby}";
	}

	private bool isPlayerNetworkingNow()
	{
		return
		NetworkClient.isConnected ||
		NetworkClient.isConnecting;
	}

	// TODO: Use round robin system for creating a room code?
	private void GenLobbyID()
	{
		m_LobbyID = UnityEngine.Random.Range(0, 99999);
	}

	private void UpdateLobbyID()
	{
		this.m_RoomID_txt.text = $"<color=#0d3aa9>{this.m_LobbyID}</color>";
	}
	#endregion
}

/*
	Enter ID -> connect to server -> parse a Dictionary for the ID given -> if ID is valid, add player to this lobby
	



*/