
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class ComNetManager : NetworkManager
{
	// Temp
	[SerializeField]
	private Transform tempSpawnLoc = null;
	//


	public void OnSceneChangeReady()
	{
		// Note: we don't want the player prefab to be active when
		// initialized to ensure that the OnEnable/Awake/Update
		// messages aren't sent before we've initialized the player's
		// equipset.
		//
		playerPrefab.SetActive(false);
		// NetworkServer.RegisterHandler<CreatePlayerMessage>(OnCreatePlayer);
		
		
	}

	// TODO: Use an event to tell the player when to do this
	// public override void OnClientConnect(NetworkConnection conn)
	// {
	// 	base.OnClientConnect(conn);

	// 	CreatePlayerMessage createPlayer;
	// 	createPlayer.name = GameApp.GetUserName();
	// 	createPlayer.equipSet = GameApp.GetEquipSet();

	// 	conn.Send(createPlayer);
	// }



	[Client]
	public GameObject GetLocalPlayer()
	{
		// TODO: Need to work on this, currently this function might be called before
		// OnCreatePlayer() causing it to return null.
		//
		if (NetworkClient.localPlayer == null)
		{
			Debug.LogWarning("Attempting to get local player, but has not been initialized yet!");
			return null;
		}

		return NetworkClient.localPlayer.gameObject;
	}

	
}
