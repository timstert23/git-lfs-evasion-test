﻿
using UnityEngine;

[AddComponentMenu("TPS/TPSSimpleCamera")]
public class TPSSimpleCameraScript : BaseCameraScript
{
	private Camera camCache;

	private TPSSimpleCameraScript.enTPSCameraFOVType m_FOVType;

	public const float CAMERA_PLAYERDEAD_FOV = 90f;

	public float m_AngleBetweenCameraAndPlayer;

	public Vector3 m_CameraDistanceWhenZoomInShoot;

	public Vector3 m_CameraDistanceWhenDead;

	private float m_CameraDistanceFromPlayerXZ;

	private float m_FireZoomTimer = -1f;

	private float m_FireZoomTime = 0.2f;

	private float m_MaxZoomFov = 0.3f;

	private float m_HittedRotateTimer = -1f;

	private float m_HittedRotateTime = 0.2f;

	private float m_MaxRotateAngle = 1f;

	private bool forceSetAngleH;

	public float GetCameraDistanceFromPlayerXZ()
	{
		return this.m_CameraDistanceFromPlayerXZ;
	}

	public override void Init(Transform playerTransform)
	{
		base.Init(playerTransform);
		this.camCache = GetComponent<Camera>();
		this.camCache.nearClipPlane = 0.3f;
		this.allowReticleMove = false;
		this.m_CameraDistanceWhenDead = new Vector3(0f, 10f, -10f);
		this.SetFOVType(TPSSimpleCameraScript.enTPSCameraFOVType.NormalFOVType, 0f);
	}

	// public override global::CameraType GetCameraType()
	// {
	// 	return global::CameraType.TPSCamera;
	// }

	public void SetFOVType(TPSSimpleCameraScript.enTPSCameraFOVType camera_type, float targetFov = 0f)
	{
		this.m_FOVType = camera_type;
		switch (this.m_FOVType)
		{
		case TPSSimpleCameraScript.enTPSCameraFOVType.NormalFOVType:
			this.m_TargetFOV = this.CAMERA_NORMAL_FOV;
			break;
		case TPSSimpleCameraScript.enTPSCameraFOVType.SniperZoomInFOVType:
			if (targetFov != 0f)
			{
				this.m_TargetFOV = targetFov;
			}
			break;
		case TPSSimpleCameraScript.enTPSCameraFOVType.Sniper1ZoomInFOVType:
			if (targetFov != 0f)
			{
				this.m_TargetFOV = targetFov;
			}
			break;
		case TPSSimpleCameraScript.enTPSCameraFOVType.FireZoomInFOVType:
			this.m_TargetFOV = this.CAMERA_ZOOMINSHOOT_FOV;
			break;
		case TPSSimpleCameraScript.enTPSCameraFOVType.AIM_FOVType:
			this.m_TargetFOV = this.CAMERA_AIM_FOV;
			break;
		case TPSSimpleCameraScript.enTPSCameraFOVType.DeadFOVType:
			this.m_TargetFOV = CAMERA_PLAYERDEAD_FOV;
			break;
		default:
			this.m_TargetFOV = this.CAMERA_NORMAL_FOV;
			break;
		}
	}

	public bool IsSniperFOVComplete()
	{
		bool result = false;
		if ((this.m_FOVType == TPSSimpleCameraScript.enTPSCameraFOVType.SniperZoomInFOVType || this.m_FOVType == TPSSimpleCameraScript.enTPSCameraFOVType.Sniper1ZoomInFOVType) && Mathf.Abs(this.camCache.fieldOfView - this.m_TargetFOV) <= 0.1f)
		{
			result = true;
		}
		return result;
	}

	public void FireZoom()
	{
	}

	public void HittedRotate()
	{
		this.m_HittedRotateTimer = 0f;
		this.m_MaxRotateAngle = 1f;
	}

	public void ExplodeRotate()
	{
		this.m_HittedRotateTimer = 0f;
		this.m_MaxRotateAngle = 3f;
	}

	public bool IsForceSetAngleH
	{
		get
		{
			return this.forceSetAngleH;
		}
	}

	public void ForceSetAngleH(bool isForceSet, float value)
	{
		this.forceSetAngleH = isForceSet;
		if (isForceSet)
		{
			float num = value - -5f;
			this.angelH = num - this.m_AngleBetweenCameraAndPlayer;
		}
	}

	// public override void CreateScreenBlood(float damage)
	// {
	// 	if (this.bs != null)
	// 	{
	// 		this.bs.NewBlood(damage);
	// 	}
	// }

	public override float GetCameraPitch()
	{
		float num = this.cameraTransform.eulerAngles.x + -3f;
		if (num > 270f)
		{
			num = -(360f - num);
		}
		return num;
	}

	private void Awake()
	{
		this.cameraTransform = Camera.main.transform;
	}

	private void Start()
	{
		this.angelV = -3f;
	}

	private void LateUpdate()
	{
		if (!this.started || this.mTarget == null)
			return;

		this.deltaTime = Time.deltaTime;
		float x = this.m_CameraRotation.x;
		float y = this.m_CameraRotation.y;
		base.CameraRotation = Vector2.zero;
		
		this.UpdateReticleCameraRotation(x, y);

		if (!this.forceSetAngleH)
		{
			this.angelH += x * this.deltaTime * this.cameraSwingSpeed;
		}
		this.angelV += y * this.deltaTime * this.cameraSwingSpeed;
		this.angelV = Mathf.Clamp(this.angelV, this.minAngelV, this.maxAngelV);
		
		/* Handles a bunch of things like rotation, FOV's, etc */
		this.UpdateGenericCameraRotation();
		this.resetLocalDeltaTime();
	}

	#region  Camera rotation
	private void UpdateReticleCameraRotation(float x, float y)
	{
		if (this.allowReticleMove)
		{
			float num = this.reticlePosition.x - (float)(Screen.width / 2);
			if (Mathf.Abs(num) < this.reticleLogoRange * (float)Screen.width || num * x < 0f)
			{
				this.reticlePosition = new Vector2(this.reticlePosition.x + x * this.reticleMoveSpeed, this.reticlePosition.y);
				if (this.limitReticle)
				{
					if ((this.reticlePosition.y > 40f || y <= 0f) && (this.reticlePosition.y <= 310f || y >= 0f))
					{
						this.reticlePosition = new Vector2(this.reticlePosition.x, this.reticlePosition.y - y * this.reticleMoveSpeed);
					}
				}
				else
				{
					this.reticlePosition = new Vector2(this.reticlePosition.x, this.reticlePosition.y - y * this.reticleMoveSpeed);
				}
			}
			else
			{
				if (!this.forceSetAngleH)
				{
					this.angelH += x * this.deltaTime * this.cameraSwingSpeed;
				}
				this.reticlePosition = new Vector2(this.reticlePosition.x, this.reticlePosition.y - y * this.reticleMoveSpeed);
				this.angelV = this.fixedAngelV;
			}
		}
	}

	private void UpdateGenericCameraRotation()
	{
				//Idk what this does so I don't have a name for it
		float num4 = 0f;
		if (this.m_HittedRotateTimer >= 0f)
		{
			this.m_HittedRotateTimer += this.deltaTime;
			if (this.m_HittedRotateTimer <= this.m_HittedRotateTime)
			{
				num4 = Mathf.Sin(this.m_HittedRotateTimer / this.m_HittedRotateTime * Mathf.PI * 2f) * this.m_MaxRotateAngle;
			}
			else
			{
				this.m_HittedRotateTimer = -1f;
			}
		}
		this.cameraTransform.rotation = Quaternion.Euler(-(this.angelV), this.angelH, num4);
		// if (this.gameScene.PlayingState == PlayingState.GamePlaying)
		{
			this.m_VirtualPlayer.rotation = Quaternion.Euler(0f, this.angelH + this.m_AngleBetweenCameraAndPlayer, 0f);
			this.m_VirtualPlayer.position = this.mTarget.position;
			Vector3 position = this.cameraDistanceFromPlayerWhenIdle;
			if (this.m_FOVType == TPSSimpleCameraScript.enTPSCameraFOVType.FireZoomInFOVType)
			{
				position = this.m_CameraDistanceWhenZoomInShoot;
			}
			else if (this.m_FOVType == TPSSimpleCameraScript.enTPSCameraFOVType.SpiderRobotFOVType)
			{
				position = this.cameraDistanceFromItem_SpiderRobot;
			}
			else if (this.m_FOVType == TPSSimpleCameraScript.enTPSCameraFOVType.RemoteAircraftFOVType)
			{
				position = this.cameraDistanceFromItem_RemoteAircraft;
			}
			Vector3 virtualPlayerTransfrom = this.m_VirtualPlayer.TransformPoint(position);
			Vector2 positionAsVec2 = new Vector2(position.y, position.z);
		
			float mag_x_sine = positionAsVec2.magnitude * Mathf.Sin(this.cameraTransform.eulerAngles.x * 0.0174532924f);
			float mag_x_cosine = -positionAsVec2.magnitude * Mathf.Cos(this.cameraTransform.eulerAngles.x * 0.0174532924f);

			virtualPlayerTransfrom = this.m_VirtualPlayer.TransformPoint(new Vector3(position.x, position.y + mag_x_sine, mag_x_cosine));
			Vector3 vector3 = this.m_VirtualPlayer.position + (Vector3.up * 3);

			Vector3 direction = virtualPlayerTransfrom - vector3;
			Ray ray = new Ray(vector3, direction);
			float maxDistance = Vector3.Distance(virtualPlayerTransfrom, vector3);
			RaycastHit raycastHit;
			int layerMask = (1 << 14) | (1 << 10) | (1 << 6); // Currently we'll just include layer six
			if (Physics.Raycast(ray, out raycastHit, maxDistance, layerMask))
			{
				float raycastDistance = Vector3.Distance(raycastHit.point, vector3);
				Vector3 raycastPoint = raycastHit.point;
				if (raycastDistance > 0.1f)
				{
					raycastPoint = ray.GetPoint(raycastDistance - 0.1f);
				}
				else
				{
					raycastPoint = vector3;
				}
				this.moveTo = new Vector3(Mathf.Lerp(this.moveTo.x, raycastPoint.x, 0.5f), Mathf.Lerp(this.moveTo.y, raycastPoint.y, 0.5f), Mathf.Lerp(this.moveTo.z, raycastPoint.z, 0.5f));
			}
			else
			{
				this.moveTo = virtualPlayerTransfrom;
			}
			this.cameraTransform.position = this.moveTo;
			float tmp_targetFov = this.m_TargetFOV;
			if (this.m_FOVType == TPSSimpleCameraScript.enTPSCameraFOVType.SniperZoomInFOVType)
			{
				if (Mathf.Abs(this.camCache.fieldOfView - this.m_TargetFOV) > 0.1f)
				{
					tmp_targetFov = Mathf.Lerp(this.camCache.fieldOfView, this.m_TargetFOV, 1f);
				}
			}
			else if (this.m_FOVType == TPSSimpleCameraScript.enTPSCameraFOVType.Sniper1ZoomInFOVType)
			{
				if (Mathf.Abs(this.camCache.fieldOfView - this.m_TargetFOV) > 0.1f)
				{
					tmp_targetFov = Mathf.Lerp(this.camCache.fieldOfView, this.m_TargetFOV, 0.2f);
				}
			}
			else if (this.m_FOVType != TPSSimpleCameraScript.enTPSCameraFOVType.DeadFOVType)
			{
				if (this.m_FireZoomTimer >= 0f)
				{
					this.m_FireZoomTimer += this.deltaTime;
					if (this.m_FireZoomTimer <= this.m_FireZoomTime)
					{
						if (this.m_FireZoomTimer <= this.m_FireZoomTime / 2f)
						{
							tmp_targetFov -= this.m_FireZoomTimer / (this.m_FireZoomTime / 2f) * this.m_MaxZoomFov;
						}
						else
						{
							tmp_targetFov -= this.m_MaxZoomFov - this.m_FireZoomTimer / this.m_FireZoomTime * this.m_MaxZoomFov;
						}
					}
					else
					{
						this.m_FireZoomTimer = -1f;
					}
				}
			}
			this.camCache.fieldOfView = tmp_targetFov;
		}
		// else
		// {
		// 	this.minAngelV = -70f;
		// 	this.maxAngelV = 70f;
		// 	this.cameraTransform.position = this.mTarget.TransformPoint(3f * Mathf.Sin(Time.time * 0.3f), 4f, 3f * Mathf.Cos(Time.time * 0.3f));
		// 	this.cameraTransform.LookAt(this.mTarget);
		// }
		this.m_CameraDistanceFromPlayerXZ = Vector2.Distance(new Vector2(this.m_VirtualPlayer.position.x, this.m_VirtualPlayer.position.z), new Vector2(this.cameraTransform.position.x, this.cameraTransform.position.z));
	}

	private void resetLocalDeltaTime()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
		{
			this.deltaTime = 0f;
		}
	}
	#endregion

	public int MyProperty { get; set; }

	public enum enTPSCameraFOVType
	{
		NormalFOVType,
		SniperZoomInFOVType,
		Sniper1ZoomInFOVType,
		FireZoomInFOVType,
		AIM_FOVType,
		DeadFOVType,
		SpiderRobotFOVType,
		RemoteAircraftFOVType
	}
}
