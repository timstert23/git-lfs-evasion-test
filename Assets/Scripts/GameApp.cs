
using UnityEngine;

public class GameApp : MonoBehaviour
{
	[SerializeField]
	private GameObject avatarPrefab;

	[SerializeField]
	private GameObject weaponPrefab;

	[SerializeField]
	private string userName;

	// TODO: GameApp owns a reference to the game UI
	// but maybe that's too closely coupled?
	//
	[SerializeField]
	private GameUI gameUI;

	private static GameApp Instance;

	private EquipSet m_EquipSet;

	private MatchNetworkManager m_NetManager;
	private Weapon m_weapon;

	#region Game logic and utils
	public static GameObject LoadPlayer(ref EquipSet m_EquipSet, ref Weapon m_Weapon, GameObject _base)
	{
		GameObject avatarPrefab = PrefabCache.Get<GameObject>("Prefabs/" + m_EquipSet.avatarPrefabName);
		GameObject weaponPrefab = PrefabCache.Get<GameObject>("Prefabs/" + m_EquipSet.weaponPrefabName);

		m_Weapon = Instantiate(weaponPrefab).GetComponent<Weapon>();

		Instance.m_weapon = m_Weapon;
		return avatarPrefab;
	}

	public static EquipSet GetEquipSet()
	{
		return Instance.m_EquipSet;
	}

	public static MatchNetworkManager GetNetManager()
	{
		return Instance.m_NetManager;
	}

	public static string GetUserName()
	{
		return Instance.userName;
	}

	public static GameUI GetGameUI()
	{
		return Instance.gameUI;
	}

	public static Weapon GetWeaponScript()
	{
		return Instance.m_weapon;
	}
	#endregion

	void OnSceneReady()
	{
		if (Instance.gameUI == null)
		{
			Instance.gameUI = GameObject.Find("GameUI").GetComponent<GameUI>();
		}
	}

	void Awake()
	{
		if (Instance == null)
			Instance = this;

		ComSceneLoadedNotifier.sceneIsReady += OnSceneReady;

		m_NetManager = GetComponent<MatchNetworkManager>();

		m_EquipSet = new EquipSet();
		m_EquipSet.avatarPrefabName = avatarPrefab.name;
		m_EquipSet.weaponPrefabName = weaponPrefab.name;
	}
}
