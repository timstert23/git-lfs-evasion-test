﻿
using UnityEngine;

public abstract class BaseCameraScript : MonoBehaviour
{
	protected float angelH;

	protected float angelV;

	protected float lastUpdateTime;

	protected float deltaTime;

	protected Transform m_VirtualPlayer;

	protected Transform mTarget;

	// public GameScene gameScene;

	public Vector3 cameraDistanceFromPlayerWhenIdle;

	public Vector3 cameraDistanceFromPlayerWhenAimed;

	public Vector3 cameraDistanceFromItem_SpiderRobot;

	public Vector3 cameraDistanceFromItem_RemoteAircraft;

	public float cameraSwingSpeed = 40f;

	public float minAngelV;

	public float maxAngelV;

	public float fixedAngelV;

	public bool isAngelVFixed;

	public bool limitReticle;

	public bool allowReticleMove;

	public float reticleLogoRange = 0.15f;

	public float reticleMoveSpeed = 20f;

	public float mutipleSizeReticle;

	protected GameObject[] lastTransparentObjList = new GameObject[5];

	protected Vector3 moveTo;

	protected bool behindWall;

	public Vector3 cameraDistanceFromPlayer;

	public bool lastInWall;

	// protected ScreenBloodScript bs;

	protected bool started;

	public float CAMERA_AIM_FOV = 22f;

	public float CAMERA_NORMAL_FOV = 38f;

	public float CAMERA_SNIPERZOOMIN_FOV = 15f;

	public float CAMERA_ZOOMINSHOOT_FOV = 22f;

	protected float m_TargetFOV = 38f;

	protected Vector2 reticlePosition;

	protected Transform cameraTransform;

	// protected global::CameraType cameraType;

	public AudioSource loseAudio;

	public Vector2 m_CameraRotation = new Vector2(0f, 0f);

	public static float m_CameraSensitivity = 0.3f;

	public Transform Target
	{
		get
		{
			return this.mTarget;
		}
		set
		{
			this.mTarget = value;
		}
	}

	public Transform CameraTransform
	{
		get
		{
			return this.cameraTransform;
		}
	}

	public Vector2 ReticlePosition
	{
		get
		{
			return this.reticlePosition;
		}
		set
		{
			this.reticlePosition = value;
		}
	}

	public Vector2 CameraRotation
	{
		get
		{
			return this.m_CameraRotation;
		}
		set
		{
			this.m_CameraRotation = value;
		}
	}

	// public abstract global::CameraType GetCameraType();

	public virtual void Init(Transform playerTransform)
	{
		// this.gameScene = GameApp.GetInstance().GetGameScene();
		// GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(PrefabCache.Load<GameObject>("Misc/VirtualPlayer"));
		this.m_VirtualPlayer = playerTransform;
		this.cameraDistanceFromPlayer = this.cameraDistanceFromPlayerWhenIdle;
		Cursor.visible = true;
		this.reticlePosition = new Vector3((float)(Screen.width / 2), (float)(Screen.height / 2), 0f);
		float[] array = new float[32];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = 100f;
		}
		base.GetComponent<Camera>().fieldOfView = this.CAMERA_NORMAL_FOV;
		this.started = true;
		this.cameraDistanceFromItem_SpiderRobot = new Vector3(1.03f, 0.77f, -3.78f);
		this.cameraDistanceFromItem_RemoteAircraft = new Vector3(1.45f, -0.21f, -3.36f);
	}

	public virtual void CreateScreenBlood(float damage)
	{
	}

	public virtual void ShowScreenBlood()
	{
		Transform transform = base.gameObject.transform.Find("Screen_Blood_Dead");
		if (transform != null)
		{
			transform.gameObject.SetActive(true);
		}
	}

	public virtual void ZoomIn(float deltaTime)
	{
	}

	public virtual void ZoomOut(float deltaTime)
	{
	}

	public virtual float GetCameraPitch()
	{
		return 0f;
	}

	public virtual Transform GetVirtualPlayer()
	{
		return this.m_VirtualPlayer;
	}

	public virtual void SetTarget(Transform trans)
	{
		this.mTarget = trans;
		if (this.mTarget != null)
		{
			this.angelH = this.mTarget.rotation.eulerAngles.y;
			base.transform.position = this.mTarget.TransformPoint(this.cameraDistanceFromPlayer);
			base.transform.rotation = Quaternion.Euler(-this.angelV, this.angelH + 15f, 0f);
		}
	}

	public Transform GetTarget()
	{
		return this.mTarget;
	}
}
