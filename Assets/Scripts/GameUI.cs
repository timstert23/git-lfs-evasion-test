
using UnityEngine;

public class GameUI : MonoBehaviour
{
	public UIJoystick m_Joystick;

	public UICameraView m_CameraController;

	public UIFireButton m_FireButton;
}
