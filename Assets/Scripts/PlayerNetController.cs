
using Mirror;
using UnityEngine;

public class PlayerNetController : NetworkBehaviour
{
    private Player m_Player;

	[SyncVar]
	private EquipSet m_EquipSet;

	// Note: Can't use Start() or Awake() since the necessary SyncVars might not 
	// have been initialized yet. Instead we have to use OnStartServer and
	// OnStartClient().
	//
	public override void OnStartClient()
	{
		m_Player = GetComponent<Player>();

		// Only initialize the player as long as they are not the host
		// (Since the host's player is already initialized by the server).
		// This serves no purpose in a dedicated server model.
		//
		if (isClientOnly)
			m_Player.Init(m_EquipSet);
	}

	public override void OnStartLocalPlayer()
	{
		gameObject.AddComponent<PlayerUIController>();
	}

	[Server]
	public void Init(EquipSet equipSet)
	{
        m_EquipSet = equipSet;
		
		m_Player = GetComponent<Player>();

		m_Player.Init(equipSet);
	}

	// Note: To reduce input lag, we should not attempt to sync movement with
	// local player and instead should directly modify it.
	//
	[Command]
	public void SyncAnimationState(int animationState, bool syncWithLocalPlayer)
	{
		RPCAnimationState(animationState, syncWithLocalPlayer);
	}

	[ClientRpc]
	private void RPCAnimationState(int animationState, bool syncWithLocalPlayer)
	{
		if (!syncWithLocalPlayer && isLocalPlayer)
			return;

		m_Player.ChangeAnimationState(animationState);
	}

	[Command]
	public void SyncLookAt(Vector3 lookAt, bool syncWithLocalPlayer)
	{
		// This will be used by new players to correctly instantiate 
		// currently connected players' lookAt
		//
		m_Player.m_InitialLookAt = lookAt;
        
		RPCLookAt(lookAt, syncWithLocalPlayer);
	}

	[ClientRpc]
	private void RPCLookAt(Vector3 lookAt, bool syncWithLocalPlayer)
	{
		if (!syncWithLocalPlayer && isLocalPlayer)
			return;

		m_Player.LookAt(lookAt);
	}

	[Command]
	public void SyncWeaponFire(bool syncWithLocalPlayer)
	{
		RPCWeaponFire(syncWithLocalPlayer);
	}

	[ClientRpc]
	private void RPCWeaponFire(bool syncWithLocalPlayer)
	{
		if (!syncWithLocalPlayer && isLocalPlayer)
			return;

		m_Player.GetCurrentWeapon().Fire();
	}
}
