
using UnityEngine;
using UnityEngine.EventSystems;

public class UIFireButton : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    private Vector2 m_Delta = Vector2.zero;

    private bool m_IsDown = false;
    private bool m_IsReleased = false;

	public virtual void OnPointerDown(PointerEventData eventData)
	{
		m_IsDown = true;
	}

	public void OnDrag(PointerEventData eventData)
	{
		m_Delta += eventData.delta;
	}

	public virtual void OnPointerUp(PointerEventData eventData)
	{
		m_IsDown = false;
		m_IsReleased = true;
	}

    public bool IsDown()
    {
        return m_IsDown;
    }

    public bool IsReleased()
    {
        bool released = m_IsReleased;
		m_IsReleased = false;
        return released;
    }

	public Vector2 GetDelta()
	{
		Vector2 delta = m_Delta;
		m_Delta = Vector2.zero;
		return delta;
	}
}
