
using UnityEngine;

public class AvatarCreator
{
	public static void Mount(GameObject avatarObj, GameObject partObj, Vector3 offset)
	{
		partObj.transform.parent = avatarObj.transform;
		partObj.transform.localPosition = offset;
	}

	public static void MountMultiSkinnedMesh(GameObject player, GameObject avatar)
	{
		foreach (SkinnedMeshRenderer skinnedMesh in avatar.GetComponentsInChildren<SkinnedMeshRenderer>())
		{
			GameObject gameObject = new GameObject(skinnedMesh.name);
			gameObject.layer = avatar.layer;
			
			Mount(player, gameObject, Vector3.zero);
			
			SkinnedMeshRenderer newSkinnedMesh = gameObject.AddComponent<SkinnedMeshRenderer>();
			newSkinnedMesh.sharedMesh = skinnedMesh.sharedMesh;
			newSkinnedMesh.sharedMaterials = skinnedMesh.sharedMaterials;
			
			Transform[] bones = new Transform[skinnedMesh.bones.Length];
			for (int i = 0; i < skinnedMesh.bones.Length; ++i)
			{
				Transform newTransform = null;
				
				foreach (Transform transform in player.GetComponentsInChildren<Transform>())
				{
					if (transform.name == skinnedMesh.bones[i].name)
					{
						newTransform = transform;
						break;
					}
				}

				bones[i] = newTransform;
			}

			newSkinnedMesh.bones = bones;
		}
	}
}