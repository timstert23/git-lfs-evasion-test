
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIJoystick : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
	[SerializeField]
	private RectTransform m_Handle;

	[SerializeField]
	private RectTransform m_Background;

	[SerializeField]
	private float m_HandleRange = 1f;
	
	private Canvas m_Canvas;

	private Camera cam;

	private Image m_BaseCollider;

	private RectTransform m_BaseRect;

	private Vector3 m_OriginalPos;
	private Vector2 m_Dir = Vector2.zero;
	private Vector2 m_Delta = Vector2.zero;

	private void Start()
	{
		m_BaseRect = GetComponent<RectTransform>();
		m_BaseCollider = GetComponent<Image>();
		m_Canvas = GetComponentInParent<Canvas>();

		Vector2 center = new Vector2(0.5f, 0.5f);
		m_Background.pivot = center;
		m_Handle.anchorMin = center;
		m_Handle.anchorMax = center;
		m_Handle.pivot = center;
		m_Handle.anchoredPosition = Vector2.zero;

		m_OriginalPos = m_Background.anchoredPosition;
	}

	public Vector2 GetDragDelta()
	{
		return m_Dir;
	}

	public virtual void OnPointerDown(PointerEventData eventData)
	{
		// Vector2 newPos = touch.position;
		// m_Background.position = new Vector3(newPos.x, newPos.y, Camera.main.nearClipPlane);
		m_Background.anchoredPosition = ScreenPointToAnchoredPosition(eventData.position);
		m_BaseCollider.enabled = false;
		OnDrag(eventData);
	}

	public void OnDrag(PointerEventData eventData)
	{
		cam = null;
		if (m_Canvas.renderMode == RenderMode.ScreenSpaceCamera)
			cam = m_Canvas.worldCamera;

		Vector2 position = RectTransformUtility.WorldToScreenPoint(cam, m_Background.position);
		Vector2 radius = m_Background.sizeDelta / 2;
		m_Dir = (eventData.position - position) / (radius * m_Canvas.scaleFactor);
		
		if (m_Dir.magnitude > 1f)
			m_Dir = m_Dir.normalized;

		m_Handle.anchoredPosition = m_Dir * radius * m_HandleRange;
	}

	public virtual void OnPointerUp(PointerEventData eventData)
	{
		m_Dir = Vector2.zero;
		m_Background.anchoredPosition = m_OriginalPos;
		m_Handle.anchoredPosition = Vector2.zero;
		m_BaseCollider.enabled = true;
	}

	protected Vector2 ScreenPointToAnchoredPosition(Vector2 screenPosition)
	{
		Vector2 localPoint = Vector2.zero;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle(m_BaseRect, screenPosition, cam, out localPoint))
		{
			// TODO: Uh... I guess I need to add half the width and height to the joystick in order for it to be centered.
			// have to take a closer look at this later.
			//
			Vector2 pivotOffset = m_BaseRect.pivot * m_BaseRect.rect.size;
			return localPoint - (m_Background.anchorMax * m_Background.sizeDelta) + pivotOffset;
		}
		return Vector2.zero;
	}
}
