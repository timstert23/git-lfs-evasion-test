using UnityEngine;

public class WeaponBase : MonoBehaviour
{
    [SerializeField]
	protected ParticleSystem m_ParticleSystem;

	[SerializeField]
	protected AudioSource m_Sound;

	[SerializeField]
	protected WeaponConfig m_Config;

	public void Mount(Transform character, Transform weaponPoint)
	{
		base.transform.position = character.position;
		base.transform.eulerAngles = character.eulerAngles;
		base.transform.localScale = character.localScale;
		base.transform.parent = weaponPoint;
		base.transform.localPosition = Vector3.zero;
		base.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 270f));
	}

	public void SetActive(bool active)
	{
		base.gameObject.SetActive(active);
	}

	public virtual void Fire()
	{
		this.m_ParticleSystem.Play();
		this.m_Sound.Play();
	}

	public Vector3 PerformHitDection(Vector3 orig)
	{
		RaycastHit hit;
		bool ray = Physics.Raycast(
			orig,
			base.transform.forward,
			out hit,
			this.m_Config.bulletTravelDistance
		);

		if (ray)
		{
			return hit.point;
		}

		//Error
		return Vector3.zero;
	}

	public void Unmount()
	{
		base.transform.parent = null;
	}

	public string GetAnimationName()
	{
		return this.m_Config.animationName;
	}

	public float GetSpeedDecrease()
	{
		return m_Config.speedDecrease;
	}

    public Transform GetFirePoint()
    {
        Debug.LogWarning("TODO: Make me a variable in the weapon config to avoid this expensive function call!");
        return transform.Find("Bone_Weapon/FirePoint");
    }
}
