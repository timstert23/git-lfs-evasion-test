using System;
using UnityEngine;

public class ComSceneLoadedNotifier : MonoBehaviour
{
    public static event Action sceneIsReady;

    void Start()
    {
        //Notify ComNetManager that the online scene has loaded
        sceneIsReady?.Invoke();
    }
}
