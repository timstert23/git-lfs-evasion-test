
using UnityEngine;

public class PlayerUIController : MonoBehaviour
{
	private Player m_Player;

	private PlayerNetController m_NetController;

	private UIJoystick m_Joystick;

	private UICameraView m_CameraController;

	private UIFireButton m_FireButton;

	private BaseCameraScript m_Camera;

	private Transform m_CamTransform;

	private Vector3 m_LastLookAt;

	private int m_LastAnimationState;

	private void Awake()
	{
		m_Joystick = GameApp.GetGameUI().m_Joystick;
		m_CameraController = GameApp.GetGameUI().m_CameraController;
		m_FireButton = GameApp.GetGameUI().m_FireButton;

		m_Camera = Camera.main.GetComponent<TPSSimpleCameraScript>();
		m_CamTransform = m_Camera.CameraTransform;

		m_Player = GetComponent<Player>();
		m_NetController = GetComponent<PlayerNetController>();
	}

	private void Start()
	{
		m_Camera.Init(m_Player.transform);
	}

	// Makeshift firerate (temp)
	private static float fireRatePerSec = 6.67f;

	private static float secsPerFire = 1.0f / fireRatePerSec;

	private float delta = secsPerFire;
	//

	private void Update()
	{
		// Firing
		// TODO: Don't forget to perform this check on the server
		delta += Time.deltaTime;
		bool canFire = delta >= secsPerFire;

		// TODO: This could cause input lag on slower devices since the server
		// has to sync the local player as well. Figure out a way around this.
		//
		if (m_FireButton.IsDown() && canFire)
		{
			m_NetController.SyncWeaponFire(true); // We'll sync local player too for now.
			delta = 0.0f;
		}

		// Movement and aiming
		Vector2 dir = m_Joystick.GetDragDelta();
		Vector3 vel = (m_Camera.transform.right * dir.x + m_Camera.transform.forward * dir.y);
		m_Player.Move(vel);

		int animationState = Utils.IsMoving(vel) ? 1 : 0;
		if (animationState != m_LastAnimationState)
		{
			m_LastAnimationState = animationState;
			m_Player.ChangeAnimationState(animationState);
			m_NetController.SyncAnimationState(animationState, false);
		}

		foreach (Touch touch in Input.touches)
			if (touch.phase == TouchPhase.Moved && touch.position.x > Screen.width / 2.0f)
				m_Camera.CameraRotation = touch.deltaPosition * 0.25f;

		m_Camera.SetTarget(m_Player.transform);

		Vector3 lookAt = m_CamTransform.position + m_CamTransform.forward * 100.0f;
		m_Player.LookAt(lookAt);

		if (!Utils.AreVectorsEqual(lookAt, m_LastLookAt, 0.1f)) // Should probably sync
		{
			m_NetController.SyncLookAt(lookAt, false);
			m_LastLookAt = lookAt;
		}
	}
}
