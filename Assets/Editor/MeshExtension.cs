#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
public static class MeshExtension
{
    private class Vertices
    {
        List<Vector3> verts = null;
        List<Vector2> uv1 = null;
        List<Vector2> uv2 = null;
        List<Vector2> uv3 = null;
        List<Vector2> uv4 = null;
        List<Vector3> normals = null;
        List<Vector4> tangents = null;
        List<Color32> colors = null;
        List<BoneWeight> boneWeights = null;

        public Vertices()
        {
            verts = new List<Vector3>();
        }
        public Vertices(Mesh aMesh)
        {
            verts = CreateList(aMesh.vertices);
            uv1 = CreateList(aMesh.uv);
            uv2 = CreateList(aMesh.uv2);
            uv3 = CreateList(aMesh.uv3);
            uv4 = CreateList(aMesh.uv4);
            normals = CreateList(aMesh.normals);
            tangents = CreateList(aMesh.tangents);
            colors = CreateList(aMesh.colors32);
            boneWeights = CreateList(aMesh.boneWeights);
        }

        private List<T> CreateList<T>(T[] aSource)
        {
            if (aSource == null || aSource.Length == 0)
                return null;
            return new List<T>(aSource);
        }
        private void Copy<T>(ref List<T> aDest, List<T> aSource, int aIndex)
        {
            if (aSource == null)
                return;
            if (aDest == null)
                aDest = new List<T>();
            aDest.Add(aSource[aIndex]);
        }
        public int Add(Vertices aOther, int aIndex)
        {
            int i = verts.Count;
            Copy(ref verts, aOther.verts, aIndex);
            Copy(ref uv1, aOther.uv1, aIndex);
            Copy(ref uv2, aOther.uv2, aIndex);
            Copy(ref uv3, aOther.uv3, aIndex);
            Copy(ref uv4, aOther.uv4, aIndex);
            Copy(ref normals, aOther.normals, aIndex);
            Copy(ref tangents, aOther.tangents, aIndex);
            Copy(ref colors, aOther.colors, aIndex);
            Copy(ref boneWeights, aOther.boneWeights, aIndex);
            return i;
        }
        public void AssignTo(Mesh aTarget)
        {
            if (verts.Count > 65535)
                aTarget.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
            aTarget.SetVertices(verts);
            if (uv1 != null) aTarget.SetUVs(0, uv1);
            if (uv2 != null) aTarget.SetUVs(1, uv2);
            if (uv3 != null) aTarget.SetUVs(2, uv3);
            if (uv4 != null) aTarget.SetUVs(3, uv4);
            if (normals != null) aTarget.SetNormals(normals);
            if (tangents != null) aTarget.SetTangents(tangents);
            if (colors != null) aTarget.SetColors(colors);
            if (boneWeights != null) aTarget.boneWeights = boneWeights.ToArray();
        }
    }

    public static Mesh GetSubmesh(this Mesh aMesh, int aSubMeshIndex)
    {
        if (aSubMeshIndex < 0 || aSubMeshIndex >= aMesh.subMeshCount)
            return null;

        int[] indices = aMesh.GetTriangles(aSubMeshIndex);
        Vertices source = new Vertices(aMesh);

        Vertices dest = new Vertices();
        Dictionary<int, int> map = new Dictionary<int, int>();
        int[] newIndices = new int[indices.Length];
        for (int i = 0; i < indices.Length; i++)
        {
            int o = indices[i];
            int n;
            if (!map.TryGetValue(o, out n))
            {
                n = dest.Add(source, o);
                map.Add(o, n);
            }
            newIndices[i] = n;
        }
        Mesh m = new Mesh();
        dest.AssignTo(m);
        m.triangles = newIndices;
        return m;
    }

    public static void SaveMesh(Mesh mesh, string path, bool makeNewInstance)
    {
        if (string.IsNullOrEmpty(path)) 
            return;

        Mesh meshToSave = makeNewInstance ? Object.Instantiate(mesh) : mesh;

        MeshUtility.Optimize(meshToSave);
        AssetDatabase.CreateAsset(meshToSave, path);
        AssetDatabase.SaveAssets();
    }

    [MenuItem("CONTEXT/Mesh/Extract Submeshes...")]
    public static void ExtractSubmeshes(MenuCommand menuCommand)
	{
        string path = EditorUtility.SaveFolderPanel("Destination", Application.dataPath, string.Empty);
        path = FileUtil.GetProjectRelativePath(path);

        Mesh mesh = menuCommand.context as Mesh;

        // Offset the y and rotate
        // NOTE: doesn't really have anything to do with extracting
        //       the submeshes, didn't know where else to put it at
        //       the moment.
        Vector3[] vertices = mesh.vertices;
        Vector3[] original = vertices;
        for (int i = 0; i < vertices.Length; ++i)
		{
            vertices[i].y -= 10000.0f;
            vertices[i] = Quaternion.AngleAxis(90, Vector3.right) * vertices[i];
        }
        mesh.SetVertices(vertices);
        //

        for (int i = 0; i < mesh.subMeshCount; ++i)
		{
            string name = mesh.name + "_" + i + ".asset";
            string fullname = path + "/" + name;
            SaveMesh(mesh.GetSubmesh(i), fullname, true);

            float progress = (float)i / mesh.subMeshCount;
            EditorUtility.DisplayProgressBar("Extracting submeshes...", "Saved '" + name + "'", progress);
        }

        EditorUtility.DisplayProgressBar("Extracting submeshes...", "Done!", 1.0f);
        EditorUtility.ClearProgressBar();

        mesh.SetVertices(original);
    }

    [MenuItem("CONTEXT/Meshfilter/Center Mesh Bounds")]
    public static void CenterMeshBounds(MenuCommand menuCommand)
    {
        Mesh mesh = menuCommand.context as Mesh;

        Vector3 center = Quaternion.AngleAxis(90, Vector3.right) * mesh.bounds.center;

        Vector3[] vertices = mesh.vertices;

        for (int i = 0; i < vertices.Length; ++i)
        {
            vertices[i] -= center;

            float progress = (float)i / vertices.Length;
            EditorUtility.DisplayProgressBar("Centering...", "Vertex: " + (i + 1), progress);
        }
        mesh.SetVertices(vertices);

        EditorUtility.DisplayProgressBar("Centering...", "Done!", 1.0f);
        EditorUtility.ClearProgressBar();

    }
}
#endif