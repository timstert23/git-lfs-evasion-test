#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Assets.Scripts.Utility
{
    public class MergeMeshWindow : EditorWindow
    {   
        public List<MeshFilter> meshFilters = new List<MeshFilter>();
        private SerializedObject serializedObject;
        private Vector2 listScroll;

        public void OnEnable()
        {
            this.serializedObject = new SerializedObject(this);
        }

        public void OnGUI()
        {
            listField();
            addSelectedToList();
            clearList();
            mergeMesh();
        }
        private void listField()
        {
            serializedObject.Update();
            var listPorp = this.serializedObject.FindProperty("meshFilters");

            listScroll = EditorGUILayout.BeginScrollView(this.listScroll);
            EditorGUILayout.PropertyField(listPorp, true);
            EditorGUILayout.EndScrollView();

            serializedObject.ApplyModifiedProperties();
            nullCheck();
        }
        private void nullCheck()
        {
            meshFilters.RemoveAll(t => t == null);
        }

        private void addSelectedToList()
        {
            if (GUILayout.Button("add to list"))
            {
                GameObject[] gos = Selection.gameObjects;
                foreach (GameObject go in gos)
                {
                    MeshFilter[] filters = go.GetComponentsInChildren<MeshFilter>();
                    foreach (MeshFilter filter in filters)
                        meshFilters.Add(filter);
                }
            }
        }

        private void clearList()
        {
            if (GUILayout.Button("clear list"))
                meshFilters.Clear();
        }

        private void mergeMesh()
        {
            if (GUILayout.Button("merge mesh"))
            {
                CombineInstance[] combine = new CombineInstance[this.meshFilters.Count];
                for (int i = 0; i < meshFilters.Count; i++)
                {
                    combine[i].mesh = meshFilters[i].sharedMesh;
                    combine[i].transform = Matrix4x4.identity;
                }
                
                string name = this.meshFilters[0].name.Split(' ')[0];
                Mesh mesh = new Mesh();

                mesh.CombineMeshes(combine, false);

                string fileName = EditorUtility.SaveFilePanelInProject("Export mesh file", "merge_" + name, "asset", "");
                AssetDatabase.CreateAsset(mesh, fileName);
                
                for (int i = 0; i < meshFilters.Count; i++)
				{
                    AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(meshFilters[i].sharedMesh.GetInstanceID()));
                    meshFilters[i].sharedMesh = mesh;
                }

                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }

        [MenuItem("Window/Mesh Tools/Merge and Export Mesh")]
        public static void ShowWindow()
        {
            var window = (MergeMeshWindow)GetWindow(typeof(MergeMeshWindow));
            window.minSize = new Vector2(50, 250);
            window.Show();
        }
    }
}
#endif