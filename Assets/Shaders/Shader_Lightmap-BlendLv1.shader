Shader "CoMi/Shader_Lightmap-BlendLv1"
{
    Properties
    {
        _Color("Main Color", Color) = (1.0, 1.0, 1.0, 1.0)
        _BlendTex1("BlendMap (RGBA)", 2D) = "black" { }
        _MainTex1("BlendTexA (R)", 2D) = "black" { }
        _MainTex("MainTexE", 2D) = "" { }
        _LightMapBright("Bright (RGB)", 2D) = "white" { }
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                float2 uv3 : TEXCOORD2;
                float2 uv4 : TEXCOORD3;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                float2 uv3 : TEXCOORD2;
                float2 uv4 : TEXCOORD3;
            };

            sampler2D _BlendTex1;
            sampler2D _MainTex1;
            sampler2D _MainTex;
            sampler2D _LightMapBright;
            float4 _BlendTex1_ST;
            float4 _MainTex1_ST;
            float4 _MainTex_ST;
            float4 _LightMapBright_ST;
            float4 _Color;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv  = TRANSFORM_TEX(v.uv, _BlendTex1);
                o.uv2 = TRANSFORM_TEX(v.uv2, _MainTex1);
                o.uv3 = TRANSFORM_TEX(v.uv3, _MainTex);
                o.uv4 = TRANSFORM_TEX(v.uv4, _LightMapBright);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 color = tex2D(_MainTex, i.uv3) * _Color;
                color.w = tex2D(_BlendTex1, i.uv).w;
                color = tex2D(_LightMapBright, i.uv4) * lerp(color, tex2D(_MainTex1, i.uv2), color.wwww) * 2.0;
                return color;
            }
            ENDCG
        }

    }
        FallBack "Diffuse"
}
