Shader "TUI/SpriteVC_2T"
{
    Properties
    {
        _MainTex("Base (RGB), Alpha (A)", 2D) = "white" { }
        _Color("Main Color", Color) = (1.0,1.0,1.0,1.0)
        _BackTex("Base (RGB), Alpha (A)", 2D) = "black" { }
        _ColorB("Back Color", Color) = (1.0,1.0,1.0,1.0)
    }
    SubShader
    {
        LOD 200
        Tags { "QUEUE" = "Transparent" "IGNOREPROJECTOR" = "true" "RenderType" = "Transparent" }
        ZWrite Off
        Cull Off
        Blend SrcAlpha OneMinusSrcAlpha
        ColorMask RGB
        Offset -1.0, -1.0

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                fixed4 color : COLOR;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                fixed4 color : COLOR;
            };

            sampler2D _MainTex;
            sampler2D _BackTex;
            float4 _MainTex_ST;
            float4 _BackTex_ST;
            float4 _Color;
            float4 _ColorB;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.color = v.color;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float4 diffuse = tex2D(_MainTex, i.uv) * _Color * i.color;
                float4 back = tex2D(_BackTex, i.uv) * _ColorB;
                return lerp(back, diffuse, diffuse.aaaa);
            }
            ENDCG
        }
    }
        FallBack "Diffuse"
}
