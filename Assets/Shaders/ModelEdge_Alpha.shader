Shader "CoMi/ModelEdge_Alpha"
{
    Properties
    {
        _MainTex("Texture (RGB)", 2D) = "black" { }
        _Color("Color", Color) = (1.0, 1.0, 1.0, 1.0)
        _AtmoColor("Atmosphere Color", Color) = (0.5, 0.5, 1.0, 1.0)
    }
    SubShader
    {
        Tags { "LIGHTMODE" = "Always" }
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float u2 : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _Color;
            float4 _AtmoColor;

            v2f vert(appdata v)
            {
                float4 normal = float4(v.normal, 0);
                float3 uv2 = normalize((UnityObjectToViewPos(normal)).xyz);

                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.u2 = clamp((uv2.x * uv2.x + uv2.y * uv2.y) - (uv2.z * uv2.z * 0.5), 0.0, 1.0);
                o.u2 *= o.u2;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 color = tex2D(_MainTex, i.uv) * _Color;
                return lerp(color, _AtmoColor, i.u2);
            }
            ENDCG
        }

    }
    FallBack "Diffuse"
}
