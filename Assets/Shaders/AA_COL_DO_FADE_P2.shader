Shader "CoMi/Particle/AA_COL_DO_FADE_P2"
{
    Properties
    {
        _Color("Main Color", Color) = (0.5,0.5,0.5,0.5)
        _MainTex("MainTex (RGB)", 2D) = "" { }
        _FadeTex("Fade Texture", 2D) = "white" { }
    }
    SubShader
    {
        Tags { "QUEUE" = "Transparent" }

        ZWrite Off
        Cull Front
        Blend SrcAlpha One

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                fixed4 color : COLOR;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                fixed4 color : COLOR;
            };

            sampler2D _MainTex;
            sampler2D _FadeTex;
            float4 _MainTex_ST;
            float4 _FadeTex_ST;
            float4 _Color;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.uv2 = TRANSFORM_TEX(v.uv2, _FadeTex);
                o.color = v.color;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float diffuse = tex2D(_MainTex, i.uv);
                float fade = tex2D(_FadeTex, i.uv2);
                float4 col = (_Color * i.color);
                return diffuse * fade * col * 2.0;
            }
            ENDCG
        }
    }
        FallBack "Diffuse"
}
