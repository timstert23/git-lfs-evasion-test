// Upgrade NOTE: replaced 'glstate_matrix_mvp' with 'UNITY_MATRIX_MVP'

Shader "CoMi/ElectricityVertAnim"
{
    Properties
    {
        _Color("_Color", Color) = (0.925373, 0.034529, 0.034529, 1.0)
        _Brightness_Min("_Brightness_Min", Range(0.0,3.0)) = 0.7
        _Brightness_Max("_Brightness_Max", Range(0.0,3.0)) = 2.5
        _MotionRateGlobal_Pixel("_MotionRateGlobal_Pixel", Float) = 1.0
        _ElectricitySharpness("_ElectricitySharpness", Range(0.0, 10.0)) = 6.0
        _MiddleOpacityMult("_MiddleOpacityMult", Float) = 1.0
        _MiddleOpacityFalloff("_MiddleOpacityFalloff", Float) = 2.0
        _MotionRateGlobal("_MotionRateGlobal", Float) = 1.0
        _NoiseMotionRate("_NoiseMotionRate", Range(0.0, 2.0)) = 0.65
        _NoiseFrequency("_NoiseFrequency", Range(0.0, 2.0)) = 1.0
        _NoiseScale("_NoiseScale", Range(0.0, 3.0)) = 1.0
    }
    SubShader
    {
        Tags { "QUEUE" = "Transparent" "IGNOREPROJECTOR" = "False" "RenderType" = "Transparent" }
        
        ZWrite Off
        Cull Off
        Blend One One

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float4 uv : TEXCOORD0;
                float4 uv2 : TEXCOORD1;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 uv1 : TEXCOORD1;
                float4 uv2 : TEXCOORD2;
                float4 uv3 : TEXCOORD3;
                float3 uv4 : TEXCOORD4;
            };

            float4 _Color;
			float _Brightness_Min;
			float _Brightness_Max;
			float _MotionRateGlobal_Pixel;
			float _ElectricitySharpness;
			float _MiddleOpacityMult;
			float _MiddleOpacityFalloff;
			float _MotionRateGlobal;
			float _NoiseMotionRate;
			float _NoiseFrequency;
			float _NoiseScale;

            v2f vert(appdata v)
            {
				float4 tmpvar_1 = v.uv;
				float3 shlight_2;

				float tangentSign_3;
				float3 worldTangent_4;
				float3 worldNormal_5;
				float3 tmpvar_6;
				float4 tmpvar_7;
				float4 tmpvar_8 = float4(0.0, tmpvar_1.y, 0.0, 0.0);
				float4 tmpvar_9 = frac(_Time * 0.05) * _MotionRateGlobal * float4(25.0, 25.0, 25.0, 25.0);
				float4 tmpvar_10 = (tmpvar_9 + (
					floor(tmpvar_9 * float4(1.123, 1.123, 1.123, 1.123))
					+
					floor(tmpvar_9 * float4(3.0, 3.0, 3.0, 3.0))
					* float4(8.93, 8.93, 8.93, 8.93)));

				float4 tmpvar_11 = float4(0.0, tmpvar_1.y, 0.0, 0.0);
				float4 tmpvar_12 = float4(0.0, tmpvar_1.y, 0.0, 0.0);
				float4 tmpvar_13 = float4(0.0, tmpvar_1.y, 0.0, 0.0);
				float4 tmpvar_14 = float4(0.0, tmpvar_1.y, 0.0, 0.0);
				float4 tmpvar_15 = _NoiseMotionRate * float4(0.15, 0.15, 0.15, 0.15);
				float4 tmpvar_16 = _NoiseScale * float4(5.0, 5.0, 5.0, 5.0);
				float4 tmpvar_17 = _NoiseScale * float4(0.5, 0.5, 0.5, 0.5);
				float4 tmpvar_18 = _NoiseScale * float4(0.25, 0.25, 0.25, 0.25);
				float4 tmpvar_19 = _NoiseScale * float4(1.5, 1.5, 1.5, 1.5);
				
				tmpvar_14.z = (((
					sin(((tmpvar_8 + (tmpvar_10 *
						(float4(3.0, 3.0, 3.0, 3.0) * _NoiseMotionRate)
						)) * (float4(193.1759, 193.1759, 193.1759, 193.1759) * _NoiseFrequency)))
					* tmpvar_18) + (
						(sin(((tmpvar_11 +
							(tmpvar_10 * tmpvar_15)
							) * (float4(8.567, 8.567, 8.567, 8.567) * _NoiseFrequency))) * tmpvar_16)
						+
						(sin(((tmpvar_12 +
							(tmpvar_10 * tmpvar_15)
							) * (float4(93.15, 93.15, 93.15, 93.15) * _NoiseFrequency))) * tmpvar_17)
						)) + (sin(
							((tmpvar_13 + (tmpvar_10 * (float4(-0.1051, -0.1051, -0.1051, -0.1051) * _NoiseMotionRate))) * (float4(25.31, 25.31, 25.31, 25.31) * _NoiseFrequency))
						) * tmpvar_19)).y;

				float4 tmpvar_20 = float4(0.0, tmpvar_1.y, 0.0, 0.0);
				float4 tmpvar_21 = tmpvar_10 + 2.0;
				float4 tmpvar_22 = lerp(float4(0.5, 0.5, 0.5, 0.5), float4(1.0, 1.0, 1.0, 1.0), (frac(
					(tmpvar_9 * float4(3.0, 3.0, 3.0, 3.0))
				).yyyy * frac(
					(tmpvar_9 * float4(1.123, 1.123, 1.123, 1.123))
				).yyyy));
				float4 tmpvar_23 = float4(0.0, tmpvar_1.y, 0.0, 0.0);
				float4 tmpvar_24 = float4(0.0, tmpvar_1.y, 0.0, 0.0);
				float4 tmpvar_25 = float4(0.0, tmpvar_1.y, 0.0, 0.0);
				float4 tmpvar_26 = float4(0.0, 0.0, 0.0, 0.0);
				tmpvar_26.y = (((
					(sin(((tmpvar_20 +
						(tmpvar_21 * (float4(0.256, 0.256, 0.256, 0.256) * _NoiseMotionRate))
						) * (float4(7.071, 7.071, 7.071, 7.071) * _NoiseFrequency))) * (tmpvar_16 * tmpvar_22))
					+
					(sin(((tmpvar_23 +
						(tmpvar_21 * (float4(0.190731, 0.190731, 0.190731, 0.190731) * _NoiseMotionRate))
						) * (float4(79.533, 79.533, 79.533, 79.533) * _NoiseFrequency))) * tmpvar_17)
					) + (
						sin(((tmpvar_24 + (tmpvar_21 *
							(float4(2.705931, 2.705931, 2.705931, 2.705931) * _NoiseMotionRate)
							)) * (float4(179.5317, 179.5317, 179.5317, 179.5317) * _NoiseFrequency)))
						* tmpvar_18)) + (sin(
							((tmpvar_25 + (tmpvar_21 * (float4(-0.107335, -0.107335, -0.107335, -0.107335) * _NoiseMotionRate))) * (float4(23.0917, 23.0917, 23.0917, 23.0917) * _NoiseFrequency))
						) * (tmpvar_19 + tmpvar_22))).y;
				float4 tmpvar_27 = (((tmpvar_14 + tmpvar_26) * (float4(1.0, 1.0, 1.0, 1.0) -
					abs((float4(-1.0, -1.0, -1.0, -1.0) + (v.uv.yyyy * float4(2.0, 2.0, 2.0, 2.0))))
					)) + v.vertex);
				tmpvar_7.xy = tmpvar_1.xy;
				tmpvar_7.zw = v.uv2.xy;
				float4 tmpvar_28 = float4(tmpvar_27.xyz, 1.0);
				float3 tmpvar_29 = (mul(unity_ObjectToWorld, tmpvar_27)).xyz;
				float4 v_30;
				v_30.x = unity_WorldToObject[0].x;
				v_30.y = unity_WorldToObject[1].x;
				v_30.z = unity_WorldToObject[2].x;
				v_30.w = unity_WorldToObject[3].x;
				float4 v_31;
				v_31.x = unity_WorldToObject[0].y;
				v_31.y = unity_WorldToObject[1].y;
				v_31.z = unity_WorldToObject[2].y;
				v_31.w = unity_WorldToObject[3].y;
				float4 v_32;
				v_32.x = unity_WorldToObject[0].z;
				v_32.y = unity_WorldToObject[1].z;
				v_32.z = unity_WorldToObject[2].z;
				v_32.w = unity_WorldToObject[3].z;
				float3 tmpvar_33 = normalize(((
					(v_30.xyz * v.normal.x)
					+
					(v_31.xyz * v.normal.y)
					) + (v_32.xyz * v.normal.z)));
				worldNormal_5 = tmpvar_33;
				float3x3 tmpvar_34;
				tmpvar_34[0] = unity_ObjectToWorld[0].xyz;
				tmpvar_34[1] = unity_ObjectToWorld[1].xyz;
				tmpvar_34[2] = unity_ObjectToWorld[2].xyz;
				float3 tmpvar_35 = normalize(mul(tmpvar_34, v.tangent.xyz));
				worldTangent_4 = tmpvar_35;
				float tmpvar_36 = (v.tangent.w * unity_WorldTransformParams.w);
				tangentSign_3 = tmpvar_36;
				float3 tmpvar_37 = (worldNormal_5.yzx * worldTangent_4.zxy - worldNormal_5.zxy * worldTangent_4.yzx) * tangentSign_3;
				float4 tmpvar_38;
				tmpvar_38.x = worldTangent_4.x;
				tmpvar_38.y = tmpvar_37.x;
				tmpvar_38.z = worldNormal_5.x;
				tmpvar_38.w = tmpvar_29.x;
				float4 tmpvar_39;
				tmpvar_39.x = worldTangent_4.y;
				tmpvar_39.y = tmpvar_37.y;
				tmpvar_39.z = worldNormal_5.y;
				tmpvar_39.w = tmpvar_29.y;
				float4 tmpvar_40;
				tmpvar_40.x = worldTangent_4.z;
				tmpvar_40.y = tmpvar_37.z;
				tmpvar_40.z = worldNormal_5.z;
				tmpvar_40.w = tmpvar_29.z;
				float4 tmpvar_41;
				tmpvar_41.w = 1.0;
				tmpvar_41.xyz = worldNormal_5;
				float4 normal_42;
				normal_42 = tmpvar_41;
				float3 res_43;
				float3 x_44;
				x_44.x = dot(unity_SHAr, normal_42);
				x_44.y = dot(unity_SHAg, normal_42);
				x_44.z = dot(unity_SHAb, normal_42);
				float3 x1_45;
				float4 tmpvar_46 = normal_42.xyzz * normal_42.yzzx;
				x1_45.x = dot(unity_SHBr, tmpvar_46);
				x1_45.y = dot(unity_SHBg, tmpvar_46);
				x1_45.z = dot(unity_SHBb, tmpvar_46);
				res_43 = (x_44 + (x1_45 + (unity_SHC.xyz *
					((normal_42.x * normal_42.x) - (normal_42.y * normal_42.y))
					)));
				res_43 = max(((1.055 *
					pow(max(res_43, float3(0.0, 0.0, 0.0)), float3(0.4166667, 0.4166667, 0.4166667))
					) - 0.055), float3(0.0, 0.0, 0.0));
				shlight_2 = res_43;
				tmpvar_6 = shlight_2;
				
				v2f o;
				o.vertex = UnityObjectToClipPos(tmpvar_28);
				o.uv0 = tmpvar_38;
				o.uv1 = tmpvar_39;
				o.uv2 = tmpvar_40;
				o.uv3 = tmpvar_7;
				o.uv4 = tmpvar_6;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
				float4 c_1;
				float3 tmpvar_2;
				float4 tmpvar_3 = frac(_Time * float4(0.05, 0.05, 0.05, 0.05)) * float4(25.0, 25.0, 25.0, 25.0) * _MotionRateGlobal_Pixel;
				float3 tmpvar_4 = ((_Color * (
				pow(((float4(1.0, 1.0, 1.0, 1.0) - abs(
					sin(((i.uv3.xyxy * float4(3.33, 3.33, 3.33, 3.33)) + float4(1.5, 1.5, 1.5, 1.5)))
				).xxxx) * lerp(_Brightness_Min, _Brightness_Max, frac(tmpvar_3 * float4(3.0, 3.0, 3.0, 3.0).yyyy
					*
					frac(tmpvar_3 * float4(1.123, 1.123, 1.123, 1.123)).yyyy))), float4(_ElectricitySharpness, _ElectricitySharpness, _ElectricitySharpness, _ElectricitySharpness))
				*
				sin(i.uv3.xyxy * float4(3.11, 3.11, 3.11, 3.11)).yyyy)) * 
					lerp(float4(_MiddleOpacityMult, _MiddleOpacityMult, _MiddleOpacityMult, _MiddleOpacityMult), float4(1.0, 1.0, 1.0, 1.0), pow(
				abs(float4(-1.0, -1.0, -1.0, -1.0) + i.uv3.yyyy * float4(2.0, 2.0, 2.0, 2.0))
				, float4(_MiddleOpacityFalloff, _MiddleOpacityFalloff, _MiddleOpacityFalloff, _MiddleOpacityFalloff)))).xyz;
				tmpvar_2 = tmpvar_4;
				c_1.xyz = tmpvar_2;
				c_1.w = 1.0;

                return c_1;
            }
            ENDCG
        }

        }
            FallBack "Diffuse"
}
