Shader "CoMi/COM_LM"
{
    Properties
    {
        _Color("Main Color", Color) = (1.0, 1.0, 1.0, 1.0)
        _MainTex ("MainTex", 2D) = "white" {}
        _LightMap ("Lightmap (RGB)", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "QUEUE" = "Geometry" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
            };

            sampler2D _MainTex;
            sampler2D _LightMap;
            float4 _MainTex_ST;
            float4 _LightMap_ST;
            float4 _Color;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.uv2 = TRANSFORM_TEX(v.uv2, _LightMap);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 diffuse = tex2D(_MainTex, i.uv) * _Color;
                fixed4 light = tex2D(_LightMap, i.uv2);
                return diffuse * light;
            }
            ENDCG
        }
        
    }
    FallBack "Diffuse"
}
