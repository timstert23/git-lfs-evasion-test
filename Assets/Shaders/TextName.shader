Shader "Unlit/TextName"
{
    Properties
    {
        _MainTex("Alpha (A)", 2D) = "white" { }
    }
    SubShader
    {
        LOD 200
        Tags { "QUEUE" = "Transparent+100" "IGNOREPROJECTOR" = "true" "RenderType" = "Transparent" }
        ZTest Always
        ZWrite Off
        Cull Off
        Blend SrcAlpha OneMinusSrcAlpha
        Offset -1.0, -1.0

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                fixed4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.color = v.color;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float4 col;

                col.rgb = i.color.rgb;
                float4 diffuse = tex2D(_MainTex, i.uv);
                
                col.w = (i.color.a * diffuse.a);
                return col;
            }
            ENDCG
        }
    }
        FallBack "Diffuse"
}
