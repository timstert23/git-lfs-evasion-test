Shader "CoMi/Particle/AAAB_COL_DO"
{
    Properties
    {
        _Color("Main Color", Color) = (0.5, 0.5, 0.5, 0.5)
        _MainTex("Particle Texture", 2D) = "white" { }
        _Color2("Main Color 2", Color) = (0.5, 0.5, 0.5, 0.5)
        _MainTex2("Particle Texture 2", 2D) = "white" { }
    }
        SubShader
    {
        Tags { "QUEUE" = "Transparent" }

        ZWrite Off
        Cull Off
        Blend SrcAlpha One

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                fixed4 color : COLOR;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                fixed4 color : COLOR;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _Color;
            float4 _Color2;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.color = v.color;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float4 col = (_Color * i.color);
                return tex2D(_MainTex, i.uv) * col * 2.0;
            }
            ENDCG
        }
    }
        FallBack "Diffuse"
}
