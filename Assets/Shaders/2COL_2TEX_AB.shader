Shader "CoMi/2COL_2TEX_AB"
{
    Properties
    {
        _MainColor("Main Color", Color) = (1.0, 1.0, 1.0, 1.0)
        _MainTex("MainTex(RGB)", 2D) = "" { }
        _SkinColor("Skin Color", Color) = (1.0, 1.0, 1.0, 1.0)
        _SkinTex("SkinTex(RGB)", 2D) = "" { }
    }
    SubShader
    {
        Tags { "QUEUE" = "Transparent" }
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _SkinTex;
            float4 _MainTex_ST;
            float4 _MainColor;
            float4 _SkinColor;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float4 tex1 = tex2D(_MainTex, i.uv) * _MainColor;
                float4 tex2 = tex2D(_SkinTex, i.uv) * _SkinColor;

                return tex1 * tex2 * 4.0 * tex1.a * tex2.a + tex1;
            }
            ENDCG
        }
    }
}
